import React, { Component } from 'react';
import UsersTableContainer from './containers/UsersTableContainer'
import AddUserContainer from './containers/AddUserContainer'
import EditUserContainer from './containers/EditUserContainer'
import ErrorModalContainer from './containers/ErrorModalContainer'
import './App.css';
import { connect } from 'react-redux'
import { getUsers } from './actions/index'

class App extends Component {

  componentDidMount() {
    this.props.getUsers()
  }

  render() {
    return (
      <div className="App container">
        <UsersTableContainer/>
        <AddUserContainer/>
        <EditUserContainer/>
        <ErrorModalContainer/>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    state: state
  }
}

App = connect(
  mapStateToProps,
  {getUsers}
)(App)

export default App

