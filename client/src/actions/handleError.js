import * as actionTypes from '../constants/ActionTypes'

const errorMessages = {
  DELETE_USER_NOT_FOUND: 'Uživatel nenalezen.',
  CONNECTION: 'Chyba spojení.',
  DEFAULT: 'Neznámá chyba.',
}

/**
 * Get error message
 * @param type
 */
const getErrorMessage = (type) => {
  return `Nelze provést ${errorMessages[type] || errorMessages.DEFAULT}`;
}

/**
 * Handle error and dispatch action
 * @param dispatch
 */
const handleError = (dispatch) => (error) => {
  if (error.response) {
    if (error.response.data && error.response.data.error) {
      const message = getErrorMessage(error.response.data.error)
      dispatch({
        type: actionTypes.SHOW_ERROR, payload: { message }
      })
    } else {
      dispatch({
        type: actionTypes.SHOW_ERROR, payload: { message: getErrorMessage()}
      })
    }
  } else if (error.request) {
    dispatch({
      type: actionTypes.SHOW_ERROR, payload: { message: getErrorMessage('CONNECTION')}
    })
  } else {
    dispatch({
      type: actionTypes.SHOW_ERROR, payload: { message: getErrorMessage()}
    })
  }
}

export default handleError