import * as api from '../api'
import * as actionTypes from '../constants/ActionTypes'

import handleError from './handleError'

/**
 * Get users
 */
export const getUsers = () => (dispatch) => {
      api.getUsers().then((res)=>{
        dispatch({
          type :actionTypes.RECEIVE_USERS, payload :{users: res.data}
        })
      }).catch(handleError(dispatch))
}

/**
 * Delete user
 * @param id
 */
export const deleteUser = (id) => (dispatch) => {
  api.deleteUser(id).then(()=>{
    dispatch({
      type :actionTypes.DELETE_USER, payload :{id}
    })
  }).catch(handleError(dispatch))

}

/**
 * Change user form
 * @param values
 */
export const changeUserForm = (values) => ({
  type :actionTypes.CHANGE_USER_FORM, payload :{values}
})

/**
 * Submit add user
 */
export const submitAddUser = () => (dispatch, getState) => {
  const user = getState().userFormValues;
  api.addUser(user).then((res)=>{
    dispatch({
      type: actionTypes.RECEIVE_ADD_USER, payload: {userId: res.data.id}
    })
  }).catch(handleError(dispatch))
}

/**
 * On click add user
 */
export const onClickAddUser = () => ({
  type: actionTypes.SHOW_ADD_USER
})

/**
 * Close add user form
 */
export const closeAddUser = () => ({
  type: actionTypes.CLOSE_ADD_USER
})

/**
 * On click edit user
 * @param userId
 */
export const onClickEditUser = (userId) => ({
  type: actionTypes.SHOW_EDIT_USER, payload: { userId }
})

/**
 * Submit edit user
 */
export const submitEditUser = () => (dispatch, getState) => {
  const user = getState().userFormValues;
  api.editUser(user).then(()=>{
    dispatch({
      type: actionTypes.RECEIVE_EDIT_USER
    })
  }).catch(handleError(dispatch))
}

/**
 * Close edit user
 */
export const closeEditUser = () => ({
  type: actionTypes.CLOSE_EDIT_USER
})

/**
 * Hide error
 */
export const hideError = () => ({
  type :actionTypes.HIDE_ERROR
})