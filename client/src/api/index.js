import axios from 'axios'
import { SERVER_URL } from '../config'

export const getUsers = () => {
  return axios.get(`${SERVER_URL}/api/users`)
}

export const addUser = ({login, name}) => {
  return axios.post(`${SERVER_URL}/api/users`, {login, name})
}

export const editUser = ({id, login, name}) => {
  return axios.put(`${SERVER_URL}/api/users/${id}`, {login, name})
}

export const deleteUser = (id) => {
  return axios.delete(`${SERVER_URL}/api/users/${id}`)
}

