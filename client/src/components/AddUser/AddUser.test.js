import React from 'react';
import {shallow} from 'enzyme';
import AddUser from './index.js';

it('renders show ', () => {
  const component = shallow(
    <AddUser
      onChange={()=>{}}
      onSubmit={()=>{}}
      onClose={()=>{}}
      show={false}
      values={{login: 'Login', name: 'name'}}
    />
  );
  expect(component.equals(null)).toBeTruthy();
});

it('renders hide', () => {
  const component = shallow(
    <AddUser
      onChange={()=>{}}
      onSubmit={()=>{}}
      onClose={()=>{}}
      show={true}
      values={{login: 'Login', name: 'name'}}
    />
  );
  expect(component.equals(null)).toBeFalsy();
});
