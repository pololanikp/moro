import React from 'react'
import PropTypes from 'prop-types'
import UserForm from '../UserForm'
import FormWrap from '../FormWrap'

const AddUser = ({show, ...otherProps}) => {

  if (!show) {
    return null
  }

  return (
    <FormWrap title="Vytvoření uživatele">
      <UserForm buttonLabel="Vytvořit" {...otherProps} />
    </FormWrap>
  )
}

AddUser.propTypes = {
  show: PropTypes.bool.isRequired,
  values: PropTypes.shape({
    login: PropTypes.string,
    name: PropTypes.string
  }),
  onChange: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired,
  onClose: PropTypes.func.isRequired
}

export default AddUser