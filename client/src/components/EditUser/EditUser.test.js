import React from 'react';
import {shallow} from 'enzyme';
import EditUser from './index.js';

it('renders show ', () => {
  const component = shallow(
    <EditUser
      onChange={()=>{}}
      onSubmit={()=>{}}
      onClose={()=>{}}
      show={false}
      values={{login: 'Login', name: 'name'}}
    />
  );
  expect(component.equals(null)).toBeTruthy();
});

it('renders hide', () => {
  const component = shallow(
    <EditUser
      onChange={()=>{}}
      onSubmit={()=>{}}
      onClose={()=>{}}
      show={true}
      values={{login: 'Login', name: 'name'}}
    />
  );
  expect(component.equals(null)).toBeFalsy();
});
