import React from 'react'
import PropTypes from 'prop-types'
import UserForm from '../UserForm'
import FormWrap from '../FormWrap'

const EditUser = ({show, ...otherProps}) => {

  if (!show) {
    return null
  }

  return (
    <FormWrap title="Editace uživatele">
      <UserForm buttonLabel="Upravit" {...otherProps} />
    </FormWrap>
  )
}

EditUser.propTypes = {
  show: PropTypes.bool.isRequired,
  values: PropTypes.shape({
    login: PropTypes.string,
    name: PropTypes.string
  }),
  onChange: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired,
  onClose: PropTypes.func.isRequired
}

export default EditUser