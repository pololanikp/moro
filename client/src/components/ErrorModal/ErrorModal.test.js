import React from 'react';
import {shallow} from 'enzyme';
import ErrorModal from './index.js';

it('renders without text - hide', () => {
  const component = shallow(
    <ErrorModal onHide={()=>{}} text={null}/>
  );
  expect(component.equals(null)).toBeTruthy();
});

it('renders with text - show', () => {
  const component = shallow(
    <ErrorModal onHide={()=>{}} text={"Test text"}/>
  );
  expect(component.equals(null)).toBeFalsy();
});

it('click to hide', () => {
   let clicked = false
  const component = shallow(
    <ErrorModal onHide={()=>{clicked = true}} text={"Test text"}/>
  );

  component.find('button').simulate('click')
  expect(clicked).toBeTruthy()
});


