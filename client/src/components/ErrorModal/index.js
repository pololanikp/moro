import React from 'react'
import PropTypes from 'prop-types'

const ErrorModal = ({text, onHide}) => {

  if (text === null) {
    return null
  }

  return (<div className="error-modal">
    <div className="alert alert-danger box-shadow error-modal__message">
      <button type="button" onClick={onHide} className="close error-modal__close" data-dismiss="alert">&times;</button>
      <h4>Chyba</h4>
      {text}
    </div>
  </div>)
}

ErrorModal.propTypes = {
  text: PropTypes.string,
  onHide: PropTypes.func.isRequired
}

export default ErrorModal