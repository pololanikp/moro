import React from 'react';
import {shallow} from 'enzyme';
import FormWRap from './index.js';

it('render children and title', () => {
  const component = shallow(
    <FormWRap title="Test title">
      <span>Children</span>
    </FormWRap>
  );
  expect(component.containsMatchingElement(<span>Children</span>)).toBeTruthy();
  expect(component.text().indexOf("Test title") !== -1).toBeTruthy();
});
