import React from 'react'
import PropTypes from 'prop-types'

const FormWrap = ({children, title}) => (
  <div className="well box-shadow show-animate">
    <legend>{title}</legend>
    {children}
  </div>
)

FormWrap.propTypes = {
  children: PropTypes.node.isRequired,
  title: PropTypes.string.isRequired
}

export default FormWrap