import React from 'react'
import PropTypes from 'prop-types'

const Body = ({children}) => (
  <tbody>
    {children}
  </tbody>
)

Body.propTypes = {
  children: PropTypes.arrayOf(PropTypes.element)
}

export default Body