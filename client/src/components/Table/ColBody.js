import React from 'react'
import PropTypes from 'prop-types'

const ColBody = ({children, ...otherProps}) => (
  <td {...otherProps}>
    {children}
  </td>
)

ColBody.propTypes = {
  children: PropTypes.node
}


export default ColBody