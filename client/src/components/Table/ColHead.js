import React from 'react'
import PropTypes from 'prop-types'

const ColHead = ({children}) => (
  <th>
    {children}
  </th>
)

ColHead.propTypes = {
  children: PropTypes.node
}

export default ColHead