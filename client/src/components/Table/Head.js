import React from 'react'
import PropTypes from 'prop-types'

const Head = ({children}) => (
  <thead>
    {children}
  </thead>
)

Head.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.element),
    PropTypes.element
  ])
}


export default Head