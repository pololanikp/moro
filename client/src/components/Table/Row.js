import React from 'react'
import PropTypes from 'prop-types'

const Row = ({children}) => (
  <tr>
    {children}
  </tr>
)

Row.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.element),
    PropTypes.element
  ])
}


export default Row