import React from 'react'
import PropTypes from 'prop-types'
import cn from 'classnames'

const Table = ({children, className}) => (
  <table className={cn('table', className)}>
    {children}
  </table>
)

Table.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.element),
    PropTypes.element
  ]),
  className: PropTypes.string
}


export default Table