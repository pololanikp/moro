import _Table from './Table'
export const Table = _Table

import _Body from './Body'
export const Body = _Body

import _Row from './Row'
export const Row = _Row

import _ColBody from './ColBody'
export const ColBody = _ColBody

import _ColHead from './ColHead'
export const ColHead = _ColHead

export {default as Head} from './Head';


