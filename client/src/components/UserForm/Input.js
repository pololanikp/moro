import React from 'react'
import PropTypes from 'prop-types'

const Input = ({label, value, onChange}) => (
  <div className="form-group">
      <label className="col-sm-2 control-label">{label}</label>
      <div className="col-sm-10">
        <input
          required
          value={value}
          className="form-control"
          onChange={(e) => onChange(e.target.value)}
        />
  </div>
  </div>
)

Input.propTypes = {
  value: PropTypes.string.isRequired,
  label: PropTypes.string,
  onChange: PropTypes.func.isRequired
}

export default Input