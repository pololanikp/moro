import React from 'react';
import { shallow } from 'enzyme';
import Input from './Input.js';

it('on change and render label ', () => {
  let nextValue = ""
  const component = shallow(
    <Input
      onChange={(value) => {
        nextValue = value
      }}
      value="Value 1"
      label="Label"
    />
  );

  component.find('input').simulate('change', {target: { value: 'Value 2'}})
  expect(component.text().indexOf("Label") !== -1).toBeTruthy();
  expect(nextValue === 'Value 2').toBeTruthy()
});


