import React from 'react';
import {shallow} from 'enzyme';
import UserForm from './index.js';

it('Submit form, close form', () => {
  let nextValues = {}
  let submitted = false
  let preventDefault = false
  const component = shallow(
    <UserForm
      values={{name: 'Name', login: 'Login'}}
      onChange={(values)=>{
        nextValues = values
      }}
      onSubmit={()=>{
        submitted = true
      }}
      buttonLabel="Button label"
      onClose={()=>{}}
    />
  );
  component.find('form').simulate('submit', {preventDefault: ()=>{ preventDefault = true}})
  expect(submitted && preventDefault).toBeTruthy();
});



