import React from 'react'
import PropTypes from 'prop-types'
import Input from './Input'

export const UserForm = ({values, buttonLabel, onChange, onSubmit, onClose}) => {

  const handleChangeValue = (name, value) => onChange({...values, [name] :value})

  const handleSubmit = (e) => {
    e.preventDefault()
    onSubmit()
  }

  return (
    <form className="form-horizontal" onSubmit={(e) => handleSubmit(e)}>
      <Input onChange={(value) => handleChangeValue('login', value)} value={values.login} label="Login"/>
      <Input onChange={(value) => handleChangeValue('name', value)} value={values.name} label="Name"/>

      <div className="form-group">
        <div className="col-sm-offset-2 col-sm-10">
          <button type="submit" className="btn btn-success">{buttonLabel}</button>
          {' '}
          <button onClick={onClose} className="btn btn-default">Zavřít</button>
        </div>
      </div>

    </form>
  )
}

UserForm.propTypes = {
  values :PropTypes.shape({
    login :PropTypes.string,
    name :PropTypes.string
  }),
  buttonLabel :PropTypes.string.isRequired,
  onChange :PropTypes.func.isRequired,
  onSubmit :PropTypes.func.isRequired,
  onClose :PropTypes.func.isRequired
}

export default UserForm