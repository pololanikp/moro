import React from 'react'
import PropTypes from 'prop-types'
import { Table, Body, Head, Row, ColHead, ColBody } from '../Table'

const UsersTable = ({users, onUserDelete, showAddButton, onClickAddUser, onClickEditUser}) => {

  if (!users) {
    return null
  }

  const rows = users.map((user) => (
    <Row key={user.id}>
      <ColBody>
        {user.login}
      </ColBody>
      <ColBody>
        {user.name}
      </ColBody>
      <ColBody className="text-right">
        <div className="btn-group">
          <button className="btn btn-default btn-sm" onClick={() => onClickEditUser(user.id)}>
            <span className="glyphicon glyphicon-pencil"></span>
          </button>
          <button className="btn btn-danger btn-sm" onClick={() => onUserDelete(user.id)}>
            <span className="glyphicon glyphicon-trash"></span>
          </button>
        </div>
      </ColBody>
    </Row>
  ))

  return (
    <div>
    <div className="user-table__wrap box-shadow">
      <Table className="user-table">
        <Head>
          <Row>
            <ColHead>
              Login
            </ColHead>
            <ColHead>
              Jméno
            </ColHead>
            <ColHead>
            </ColHead>
          </Row>
        </Head>
        <Body>
        {rows}
        </Body>
      </Table>
    </div>
      {showAddButton && <div className="text-center">
        <button onClick={onClickAddUser} className="btn btn-default btn-lg">Přidat uživatele</button>
      </div>}
    </div>

  )
}

UsersTable.propTypes = {
  users :PropTypes.arrayOf(PropTypes.shape({
    login :PropTypes.string.isRequired,
    name :PropTypes.string.isRequired,
  })),
  showAddButton :PropTypes.bool.isRequired,
  onUserDelete :PropTypes.func.isRequired,
  onClickAddUser :PropTypes.func.isRequired,
  onClickEditUser :PropTypes.func.isRequired,
}

export default UsersTable