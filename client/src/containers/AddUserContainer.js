import AddUser from '../components/AddUser'
import { connect } from 'react-redux'
import { changeUserForm, submitAddUser, closeAddUser } from '../actions/index'

const mapStateToProps = (state) => {
  return {
    values: state.userFormValues,
    show: state.showAddUser,
  }
}

export default connect(
  mapStateToProps,
  { onChange: changeUserForm, onSubmit: submitAddUser, onClose: closeAddUser }
)(AddUser)

