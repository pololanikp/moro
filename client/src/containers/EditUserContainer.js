import EditUser from '../components/EditUser'
import { connect } from 'react-redux'
import { changeUserForm, submitEditUser, closeEditUser } from '../actions/index'

const mapStateToProps = (state) => {
  return {
    values: state.userFormValues,
    show: state.showEditUser
  }
}

export default connect(
  mapStateToProps,
  { onChange: changeUserForm, onSubmit: submitEditUser, onClose: closeEditUser }
)(EditUser)

