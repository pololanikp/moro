import ErrorModal from '../components/ErrorModal'
import { connect } from 'react-redux'
import { hideError } from '../actions/index'

const mapStateToProps = (state) => {
  return {
    text: state.error,
  }
}

export default connect(
  mapStateToProps,
  { onHide: hideError }
)(ErrorModal)

