import UsersTable from '../components/UsersTable'
import { connect } from 'react-redux'
import { deleteUser, onClickAddUser, onClickEditUser } from '../actions/index'

const mapStateToProps = (state) => {
  return {
    users: state.users,
    showAddButton: !state.showAddUser && !state.showEditUser
  }
}

export default connect(
  mapStateToProps,
  {onUserDelete: deleteUser, onClickAddUser, onClickEditUser}
)(UsersTable)


