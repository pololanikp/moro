import * as actionTypes from '../constants/ActionTypes'


const defaultState = {
  users :null, // array of object
  userFormValues: {name: '', login: ''},
  showAddUser: false,
  showEditUser: false,
  editUserId: null, // int
  error: null // string
}

/**
 * Sort users
 * @param users
 */
const sortUsers = (users) => users.sort((a,b) => a.id - b.id)

export default function users(state = defaultState, action) {
  switch (action.type) {
    case actionTypes.RECEIVE_USERS:
      return {...state, users :sortUsers(action.payload.users)}
    case actionTypes.DELETE_USER:
      return {...state, users :state.users.filter(user => user.id !== action.payload.id)}
    case actionTypes.CHANGE_USER_FORM:
      return {...state, userFormValues: action.payload.values}
    case actionTypes.SHOW_ADD_USER:
      return {...state, showAddUser: true, showEditUser: false}
    case actionTypes.CLOSE_ADD_USER:
      return {...state, showAddUser: false}
    case actionTypes.RECEIVE_ADD_USER:
      return {
        ...state,
        userFormValues: defaultState.userFormValues,
        users: [...state.users, {...state.userFormValues, id: action.payload.userId}],
        showAddUser: false
      }
    case actionTypes.SHOW_EDIT_USER:
      return {...state,
        showEditUser: true,
        showAddUser: false,
        userFormValues: state.users.find((user) => user.id === action.payload.userId)
      }
    case actionTypes.CLOSE_EDIT_USER:
      return {...state, showEditUser: false, userFormValues: defaultState.userFormValues}
    case actionTypes.RECEIVE_EDIT_USER:
      return {
        ...state,
        userFormValues: defaultState.userFormValues,
        users: state.users.map((user) => user.id === state.userFormValues.id ? state.userFormValues : user),
        showEditUser: false
      }
    case actionTypes.SHOW_ERROR:
      return { ...state, error: action.payload.message}
    case actionTypes.HIDE_ERROR:
      return {...state, error: null}
    default:
      return state
  }
}