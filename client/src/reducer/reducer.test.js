import reducer from './reducer'
import * as types from '../constants/ActionTypes'

it('receive users', () => {
  const action = {
    type: types.RECEIVE_USERS,
    payload: {
      users: [{id: 1, login: 'Login', name: 'Name'}]
    }
  }

  let state = reducer(undefined, action)
  expect(state.users).toEqual(action.payload.users)
})