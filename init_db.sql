CREATE TABLE public.users
(
    id INTEGER DEFAULT nextval('users_id_seq'::regclass) PRIMARY KEY NOT NULL,
    login VARCHAR(50),
    name VARCHAR(50)
);
CREATE UNIQUE INDEX users_id_pk ON public.users (id);
CREATE UNIQUE INDEX users_id_uindex ON public.users (id);

INSERT INTO public.users (name, login, id) VALUES ('Radek Malec', 'radek', 63);
INSERT INTO public.users (name, login, id) VALUES ('Petr Pololáník', 'pololanik', 66);
INSERT INTO public.users (name, login, id) VALUES ('Josef Novák', 'novak', 49);
INSERT INTO public.users (name, login, id) VALUES ('Marek Dobr', 'dobry', 67);
