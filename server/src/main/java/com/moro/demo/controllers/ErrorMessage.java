package com.moro.demo.controllers;

public class ErrorMessage {
    public enum ErrorTypes {
        DEFAULT, DELETE_USER_NOT_FOUND
    }

    ErrorTypes error;

    public ErrorMessage(ErrorTypes error) {
        this.error = error;
    }

    public ErrorTypes getError() {
        return error;
    }

    public void setError(ErrorTypes error) {
        this.error = error;
    }

    public String toString() {
        return "{errorType: " + error + "}";
    }
}