package com.moro.demo.controllers;

import com.moro.demo.errors.UserNotFoundException;
import com.moro.demo.models.User;
import com.moro.demo.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/users")
@CrossOrigin(origins = "all", maxAge = 3600)
public class RestController {

    @Autowired
    private UserService userService;

    @RequestMapping("")
    @ResponseBody
    public List<User> getUser() {
        return userService.getAll();
    }


    @RequestMapping(value = "", method = RequestMethod.POST)
    @ResponseBody
    public User create(@RequestBody User user) {
        return userService.create(user);
    }

    @DeleteMapping("/{id}")
    @ResponseBody
    public ResponseEntity<Object> deleteUser(@PathVariable Integer id) {
        try {
            userService.deleteUser(id);
        } catch (UserNotFoundException e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new ErrorMessage(ErrorMessage.ErrorTypes.DELETE_USER_NOT_FOUND));
        }
        return null;
    }

    @PutMapping("/{id}")
    @ResponseBody
    public ResponseEntity<Object> editUser(@PathVariable Integer id, @RequestBody User user) {
        try {
            userService.editUser(id, user.getLogin(), user.getName());
        } catch (UserNotFoundException e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new ErrorMessage(ErrorMessage.ErrorTypes.DELETE_USER_NOT_FOUND));
        }
        return null;
    }

}
