package com.moro.demo.models;

import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;
import java.util.List;

@Transactional
public interface UserDao extends CrudRepository<User, Long> {

    public User findByLogin(String login);

    public User findById(int id);

    public List<User> findAllByOrderByLoginAsc();

}
