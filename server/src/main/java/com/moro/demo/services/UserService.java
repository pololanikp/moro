package com.moro.demo.services;

import com.moro.demo.errors.UserNotFoundException;
import com.moro.demo.models.User;
import com.moro.demo.models.UserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
public class UserService {

    @Autowired
    private UserDao userDao;

    public User create(User user) {
        user.setId(null);
        userDao.save(user);

        return user;
    }

    public void editUser(int id, String login, String name) throws UserNotFoundException {
        if (userDao.findById(id) != null) {
            User user = new User(login, name);
            user.setId(id);
            userDao.save(user);
        } else {
            throw new UserNotFoundException();
        }
    }

    public User getByLogin(String login) {
        return userDao.findByLogin(login);
    }

    public List<User> getAll() {
        return userDao.findAllByOrderByLoginAsc();
    }

    public void deleteUser(Integer id) throws UserNotFoundException {
        User user = userDao.findById(id);
        if (user != null) {
            userDao.delete(user);
        } else {
            throw new UserNotFoundException();
        }
    }

}
